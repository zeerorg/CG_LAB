#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <GL/glut.h>
// gcc -Wall -g ellipse.c -lGL -lGLU -lglut
#define ROUND(a) ((int) (a+0.5))
int Rx, Ry, x_cen, y_cen;

void draw_point_ellipse(int x, int y) {
    glBegin(GL_POINTS);
        glVertex2d(x_cen+x, y_cen+y);
        glVertex2d(x_cen-x, y_cen+y);
        glVertex2d(x_cen+x, y_cen-y);
        glVertex2d(x_cen-x, y_cen-y);
    glEnd();
    glFlush();
    usleep(10000);
}

void Ark() {
    int Rx2 = Rx * Rx;
    int Ry2 = Ry * Ry;
    int twoRx2 = 2 * Rx2;
    int twoRy2 = 2 * Ry2;
    int p;
    int x = 0;
    int y = Ry;
    int px = 0;
    int py = twoRx2 * y;
    glClear(GL_COLOR_BUFFER_BIT);

    draw_point_ellipse(x, y);
    p = ROUND (Ry2 - (Rx2 * Ry) + (0.25 * Rx2));
    /* Region1 */
    while(px < py) {
        x++;
        px += twoRy2;
        if(p < 0)
            p += Ry2 + px;
        else {
            y--;
            py -= twoRx2;
            p += Ry2 + px - py;
        }
        draw_point_ellipse(x, y);
        //usleep(10000);
    }

    /* Region 2 */
    p = ROUND (Ry2 * (x+0.5) * (x+0.5) + Rx2*(y-1)*(y-1) - Rx2*Ry2);
    while(y > 0) {
        y--;
        py -= twoRx2;
        if(p > 0)
            p += Rx2 - py;
        else {
            x++;
            px += twoRy2;
            p += Rx2 - py + px;
        }
        draw_point_ellipse(x, y);
    }
}

void Ellipse() {
    Ark();
}

void Init() {
  glClearColor(1.0,1.0,1.0,0);
  glColor3f(0.0,0.0,0.0);
  gluOrtho2D(0 , 640 , 0 , 480);
}

int main(int argc, char **argv) {
  printf("Enter poles of ellipse to be drawn:\n");
  printf("\n************************************\n");
  scanf("%d%d",&Rx, &Ry);

  printf("Enter midpoint of ellipse to be drawn:\n");
  printf("\n***********************************\n");
  scanf("%d%d",&x_cen, &y_cen);
  
  glutInit(&argc,argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowPosition(0,0);
  glutInitWindowSize(640,480);
  glutCreateWindow("Ellipse drawing");
  Init();
  glutDisplayFunc(Ellipse);
  glutMainLoop();
  return 0;
}