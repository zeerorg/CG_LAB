<div align="center">
<h1> Practical 14 </h1>
</div>

## Aim
To write a program to draw polygons.

## Description of aim and related theory
In elementary geometry, a polygon (/ˈpɒlɪɡɒn/) is a plane figure that is bounded by a finite chain of straight line segments closing in a loop to form a closed polygonal chain or circuit. These segments are called its edges or sides, and the points where two edges meet are the polygon's vertices (singular: vertex) or corners.

## Algorithm
1. Iterate over all points entered
2. Join points one by one

## Code
```Cpp
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <GL/glut.h>
// gcc -Wall -g light.c -lGL -lGLU -lglut
float X[10], Y[10];
int num;

int round_value(float v)
{
    return (int)v + 0.5;
}

void lin(void) {
    glClear(GL_COLOR_BUFFER_BIT);
    //glLineWidth((int)thickness);
    glBegin(GL_POLYGON);
    for(int i=0; i<num; i++)
        glVertex2f(X[i], Y[i]);
    glEnd();
    glFlush();
}
void Init()
{
    glClearColor(1.0,1.0,1.0,0);
    glColor3f(0.0,0.0,0.0);
    gluOrtho2D(0 , 640 , 0 , 480);
}
int main(int argc, char **argv)
{
    printf("Enter number of points: ");
    scanf("%d", &num);
    printf("\n");
    for(int i=0; i<num; i++) {
        printf("Enter point %d: ", i);
        scanf("%g%g", &X[i], &Y[i]);
    }

    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowPosition(0,0);
    glutInitWindowSize(640,480);
    glutCreateWindow("Polygon");
    Init();
    glutDisplayFunc(lin);
    glutMainLoop();
    return 0;
}

```
## Output
![Practical 11 Output 1](https://imgur.com/69fkSAT.jpg)

## Discussion


## Finding and Learning
Implemented drawing polygons in OpenGL in C++.
