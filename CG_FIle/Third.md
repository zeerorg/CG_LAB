<div align="center">
<h1> Practical 3 </h1>
</div>

## Aim
To write a program which draws line with Bresenham's line algorithm.

## Description of aim and related theory
Bresenham's line algorithm is an algorithm that determines the points of an n-dimensional raster that should be selected in order to form a close approximation to a straight line between two points. It is commonly used to draw line primitives in a bitmap image (e.g. on a computer screen), as it uses only integer addition, subtraction and bit shifting, all of which are very cheap operations in standard computer architectures.

## Algorithm
1.  dx = x1 - x0
2.  dy = y1 - y0
3.  D = 2*dy - dx
4.  y = y0
5.  for x from x0 to x1
6.  &emsp;&emsp;plot(x,y)
7.  &emsp;&emsp;if D > 0
8.  &emsp;&emsp;&emsp;&emsp;y = y + 1
9.  &emsp;&emsp;&emsp;&emsp;D = D - 2*dx
10. &emsp;&emsp;end if
11. &emsp;&emsp;D = D + 2*dy

## Code
```C
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <GL/glut.h>

int X1, Y1, X2, Y2;

void LineBresenham() {
    int x1 = X1, y1 = Y1, x2 = X2, y2 = Y2;
    int m = 2 * (y2 - y1);
    int slope_err = m - (x2 - x1);
    glClear(GL_COLOR_BUFFER_BIT);
    for(int x=x1, y=y1; x <= x2; x++) {
        glBegin(GL_POINTS);
            glVertex2d(x, y);
        glEnd();
        glFlush();
        usleep(10000);
        slope_err += m;
        if(slope_err >= 0) {
            y++;
            slope_err -= 2 * (x2 - x1);
        }
    }
}

void Init() {
  glClearColor(1.0,1.0,1.0,0);
  glColor3f(0.0,0.0,0.0);
  gluOrtho2D(0 , 640 , 0 , 480);
}

int main(int argc, char **argv) {
  printf("Enter two end points of the line to be drawn:\n");
  printf("\n************************************");
  printf("\nEnter Point1( X1 , Y1):\n");
  scanf("%d%d",&X1,&Y1);
  printf("\n************************************");
  printf("\nEnter Point1( X2 , Y2):\n");
  scanf("%d%d",&X2,&Y2);

  glutInit(&argc,argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowPosition(0,0);
  glutInitWindowSize(640,480);
  glutCreateWindow("Bresnan Line Algo");
  Init();
  glutDisplayFunc(LineBresenham);
  glutMainLoop();
  return 0;
}
```
## Output
(0, 100) (200, 200) <br>
![Practical 3 Output 1](https://i.imgur.com/ZA2puqG.png)
<br><br>
(200, 200) (400, 350) <br>
![Practical 3 Output 2](https://i.imgur.com/CJICpeb.png)

## Discussion
Bresenham's algorithm is an improvement over DDA algorithm in that it uses only integer arithmetic. <br>
Due to this fact, the algorithm is used in hardware such as plotters and in the graphics chips of modern graphics cards. It can also be found in many software graphics libraries. Because the algorithm is very simple, it is often implemented in either the firmware or the graphics hardware of modern graphics cards.

## Finding and Learning
Implemented Bresenham Line drawing algorithm using OpenGL in C.
