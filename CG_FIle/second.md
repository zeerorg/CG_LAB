<div align="center">
<h1> Practical 2 </h1>
</div>

## Aim
To write a program which draws line with DDA line algorithm.

## Description of aim and related theory
In any 2-Dimensional plane if we connect two points (x0, y0) and (x1, y1), we get a line segment. But in the case of computer graphics we can not directly join any two coordinate points, for that we should calculate intermediate point’s coordinate and put a pixel for each intermediate point.

## Algorithm
1. dx = X1 - X0;
2. dy = Y1 - Y0;
3. steps = abs(dx) > abs(dy) ? abs(dx0 : abs(dy);
4. Xinc = dx / (float) steps;
5. Yinc = dy / (float) steps;
6. X = X0;
7. Y = Y0;
8. for (int i = 0; i <= steps; i++)
9.  &emsp;&emsp; putpixel (X,Y,WHITE);
10. &emsp;&emsp;   X += Xinc;
11. &emsp;&emsp;   Y += Yinc;

## Code
```C
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <GL/glut.h>

double X1, Y1, X2, Y2;

int round_value(float v) {
  return (int)v + 0.5;
}

void LineDDA(void) {
  double dx=(X2-X1);
  double dy=(Y2-Y1);
  double steps;
  float xInc,yInc,x=X1,y=Y1;
  steps=(abs(dx)>abs(dy))?(abs(dx)):(abs(dy));
  xInc=dx/(float)steps;
  yInc=dy/(float)steps;

  glClear(GL_COLOR_BUFFER_BIT);
    glVertex2d(x,y);
    int k;
    for(k=0;k<steps;k++) {
      x+=xInc;
      y+=yInc;
      glBegin(GL_POINTS);
      glVertex2d(round_value(x), round_value(y));
      glEnd();
      glFlush();
      usleep(10000);
    }
}
void Init() {
  glClearColor(1.0,1.0,1.0,0);
  glColor3f(0.0,0.0,0.0);
  gluOrtho2D(0 , 640 , 0 , 480);
}

int main(int argc, char **argv) {
  printf("Enter two end points of the line to be drawn:\n");
  printf("\n************************************");
  printf("\nEnter Point1( X1 , Y1):\n");
  scanf("%lf%lf",&X1,&Y1);
  printf("\n************************************");
  printf("\nEnter Point1( X2 , Y2):\n");
  scanf("%lf%lf",&X2,&Y2);

  glutInit(&argc,argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowPosition(0,0);
  glutInitWindowSize(640,480);
  glutCreateWindow("DDA_Line");
  Init();
  glutDisplayFunc(LineDDA);
  glutMainLoop();
  return 0;
}

```
## Output
(100, 200) (350, 400) <br>
![Practical 2 Output 1](https://i.imgur.com/FyWRV3w.png)
<br><br>
(0, 0) (200, 300) <br>
![Practical 2 Output 2](https://i.imgur.com/FcYX8Hm.png)

## Discussion
The DDA method can be implemented using floating-point or integer arithmetic. The native floating-point implementation requires one addition and one rounding operation per interpolated value (e.g. coordinate x, y, depth, color component etc.) and output result. This process is only efficient when an FPU with fast add and rounding operation will be available.

## Finding and Learning
Implemented DDA Line drawing algorithm using OpenGL in C.
