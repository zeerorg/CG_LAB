<div align="center">
<h1> Practical 1 </h1>
</div>

## Aim
To write a program which draws line using the provided function.

## Description of aim and related theory
Communicating with graphics hardware and display is a daunting task. To tackle this operating systems provide graphics library which have a simple API which provide different functions for common tasks (like drawing pixel, line, triangles etc.). <br>
In this practical we are using a standard graphics api (graphics.h or opengl) to understand how a graphics library works and helps us in writing graphics programs.

## Algorithm
1. initialize_display()
2. draw_line(100, 100)
3. wait_for_exit()

## Code
```C
#include <stdio.h>
#include <math.h>
#include <GL/glut.h>

int ac;
char **av;
int DETECT = 5;


void disp() {

  glClear(GL_COLOR_BUFFER_BIT);

  glBegin(GL_LINES);
    glVertex2i(100, 100);
    glVertex2i(200, 200);
  glEnd();

  glFlush();
}
void Init() {
  glClearColor(1.0,1.0,1.0,0);
  glColor3f(0.0,0.0,0.0);
  gluOrtho2D(0 , 640 , 0 , 480);
}

void intgraph(int *gd, int *gm, char *s) {
  glutInit(&ac,av);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowPosition(0,0);
  glutInitWindowSize(640,480);
  glutCreateWindow("Line");
  Init();
}

void line(int x1, int y1, int x2, int y2) {
    glutDisplayFunc(disp);
}

int main(int argc, char **argv) {
  ac = argc;
  av = argv;
  int gd = DETECT, gm = 0 ;
  intgraph(&gd, &gm, "C:\\TC\\BGI");
  line(100, 100, 200, 200);
  glutMainLoop();
  return 0;
}
```
## Output
![Practical 1 Output](https://i.imgur.com/ifpaFas.png)

## Discussion
Standard graphics API ease a lot of tasks and don't require much in depth knowledge of how the actual graphics buffers and video memory need to be maipulated. OpenGL is more robust in this regard that it needs more variables at setup but has more power and gives better results compared to other primitive APIs like graphics.h

## Finding and Learning
Learnt how to use opengl api for basic task like drawing a line.
