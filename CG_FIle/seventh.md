<div align="center">
<h1> Practical 7 </h1>
</div>

## Aim
To write a program for flood fill algorithm.

## Description of aim and related theory
Sometimes we come across an object where we want to fill the area and its boundary with different colors. We can paint such objects with a specified interior color instead of searching for particular boundary color as in boundary filling algorithm.

Instead of relying on the boundary of the object, it relies on the fill color. In other words, it replaces the interior color of the object with the fill color. When no more pixels of the original interior color exist, the algorithm is completed.

## Algorithm
1. Flood_fill(X, Y, old_color, new_color)
2. &emsp;&emsp;if get_pixel(X, Y) == old_color
3. &emsp;&emsp;&emsp;&emsp;draw_pixel(X, Y)
4. &emsp;&emsp;&emsp;&emsp;boundary_fill(X-1, Y, fill_color, boundary_color)
5. &emsp;&emsp;&emsp;&emsp;boundary_fill(X+1, Y, fill_color, boundary_color)
6. &emsp;&emsp;&emsp;&emsp;boundary_fill(X, Y-1, fill_color, boundary_color)
7. &emsp;&emsp;&emsp;&emsp;boundary_fill(X, Y+1, fill_color, boundary_color)
8. End 

## Code
```Cpp
#include <math.h>
#include <GL/glut.h>
// g++ -Wall -g newed/OpenGL-FloodFill-Circle.cpp -lGL -lGLU -lglut && ./a.out
struct Point {
	GLint x;
	GLint y;
};

struct Color {
	GLfloat r;
	GLfloat g;
	GLfloat b;
};

void init() {
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glColor3f(0.0, 0.0, 0.0);
	glPointSize(1.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, 640, 0, 480);
}

Color getPixelColor(GLint x, GLint y) {
	Color color;
	glReadPixels(x, y, 1, 1, GL_RGB, GL_FLOAT, &color);
	return color;
}

void setPixelColor(GLint x, GLint y, Color color) {
	glColor3f(color.r, color.g, color.b);
	glBegin(GL_POINTS);
	glVertex2i(x, y);
	glEnd();
	glFlush();
}

void floodFill(GLint x, GLint y, Color oldColor, Color newColor) {
	Color color;
	color = getPixelColor(x, y);

	if(color.r == oldColor.r && color.g == oldColor.g && color.b == oldColor.b)
	{
		setPixelColor(x, y, newColor);
		floodFill(x+1, y, oldColor, newColor);
		floodFill(x, y+1, oldColor, newColor);
		floodFill(x-1, y, oldColor, newColor);
		floodFill(x, y-1, oldColor, newColor);
	}
	return;
}

void onMouseClick(int button, int state, int x, int y)
{
	Color newColor = {1.0f, 0.0f, 0.0f};
	Color oldColor = {1.0f, 1.0f, 1.0f};

	floodFill(400, 300, oldColor, newColor);
}

void draw_circle(Point pC, GLfloat radius) {
	GLfloat step = 1/radius;
	GLfloat x, y;

	for(GLfloat theta = 0; theta <= 360; theta += step) {
		x = pC.x + (radius * cos(theta));
		y = pC.y + (radius * sin(theta));
		glVertex2i(x, y);
	}
}

void display(void) {
	Point pt = {400, 300};
	GLfloat radius = 100;

	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POINTS);
		draw_circle(pt, radius);
	glEnd();
	glFlush();
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
	glutInitWindowSize(640, 480);
	glutInitWindowPosition(200, 200);
	glutCreateWindow("Flood Fill");
	init();
	glutDisplayFunc(display);
	glutMouseFunc(onMouseClick);
	glutMainLoop();
	return 0;
}
```
## Output
fill_color = (0,0,1)(blue)
boundary_color = (1,0,0)(red)
![Practical 7 Output 1](https://imgur.com/osq06ur.jpg)

## Discussion
Flood fill algorithm is for filling polygons. It's a basic recursion algorithm and requires a large stack for bigger polygons. For this reason it is generally not implemented as is.

## Finding and Learning
Implemented flood fill algorithm using OpenGL in C++.
