<div align="center">
<h1> Practical 11 </h1>
</div>

## Aim
To write a program to Implement Scaling of object.

## Description of aim and related theory
To change the size of an object, scaling transformation is used. In the scaling process, you either expand or compress the dimensions of the object. Scaling can be achieved by multiplying the original coordinates of the object with the scaling factor to get the desired result.

## ALgorithm
Let us assume that the original coordinates are (X, Y), the scaling factors are (SX, SY), and the produced coordinates are (X’, Y’). This can be mathematically represented as shown below −

X' = X . SX and Y' = Y . SY

## Code
```Cpp
#include <GL/glut.h>

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

typedef struct {
    GLfloat x, y;
} Point;

const Point sqr[] = {
        {20,  30},
        {20,  80},
        {100, 80},
        {100, 30}
};

GLfloat sc_matrix[][3] = {
        {2, 0, 0},
        {0, 2, 0},
        {0, 0, 2}
};

Point multiply(GLfloat m[][3], Point v) {

    GLfloat arr[] = {v.x, v.y, 1}, ans[] = {0, 0, 1};
    int i, j;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            ans[i] += m[i][j] * arr[j];
        }
    }
    Point p = {ans[0], ans[1]};
    return p;
}

void init_graph(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    glutCreateWindow(argv[0]);
    glClearColor(1.0, 1.0, 1.0, 0.0);
    glPointSize(1.0f);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, SCREEN_WIDTH, 0, SCREEN_HEIGHT);
}

void close_graph() {
    glutMainLoop();
}

void init() {
    glClear(GL_COLOR_BUFFER_BIT);
    const int xshift = 300;
    //original
    glColor3f(0.5, 0.5, 0.5);
    glBegin(GL_LINE_LOOP);
    for (int i = 0; i < 4; ++i) {
        glVertex2f(sqr[i].x, sqr[i].y);
    }
    glEnd();

    //scaled
    glColor3f(0, 1, 0);
    glBegin(GL_LINE_LOOP);
    for (int k = 0; k < 4; ++k) {
        Point p = multiply(sc_matrix, sqr[k]);
        glVertex2f(xshift + p.x, p.y);
    }
    glEnd();
    glFlush();
}

int main(int argc, char **argv) {
    init_graph(argc, argv);
    glutDisplayFunc(init);
    close_graph();
    return EXIT_SUCCESS;
}
```
## Output
![Practical 13 Output 1](https://imgur.com/GQoEt2y.jpg)

## Discussion
If we provide values less than 1 to the scaling factor S, then we can reduce the size of the object. If we provide values greater than 1, then we can increase the size of the object.

## Finding and Learning
Implemented Scaling of object using OpenGL in C++.
