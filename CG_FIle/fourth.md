<div align="center">
<h1> Practical 4 </h1>
</div>

## Aim
To write a program which draws circle with Bresenham's circle algorithm.

## Description of aim and related theory
This algorithm draws all eight octants simultaneously, starting from each cardinal direction (0°, 90°, 180°, 270°) and extends both ways to reach the nearest multiple of 45° (45°, 135°, 225°, 315°). It can determine where to stop because when y = x, it has reached 45°. As y increases, it does not skip nor repeat any y value until reaching 45°. So during the while loop, y increments by 1 each iteration, and x decrements by 1 on occasion, never exceeding 1 in one iteration. This changes at 45° because that is the point where the tangent is rise = run. Whereas rise > run before and rise < run after.

## Algorithm
1.  Input the radius (r) and center coordinates (xc, yc)
2.  x = 0
3.  y = r
4.  d = 3 - 2 * r
5.  while y >= x
6.  &emsp;&emsp;plotCircle(xc, yc, x, y)
7.  &emsp;&emsp;x = x + 1
8.  &emsp;&emsp;if d > 0
9.  &emsp;&emsp;&emsp;&emsp;y = y -1
10.  &emsp;&emsp;&emsp;&emsp;d = d + 4 * (x - y) + 10
11. &emsp;&emsp;else
12. &emsp;&emsp;&emsp;&emsp;d = d + 4 * x + 6
13. &emsp;&emsp;end if

## Code
```C
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <GL/glut.h>
// gcc -Wall -g light.c -lGL -lGLU -lglut
int radius, x_cen, y_cen;

void draw_point_circle(int x, int y) {
    glBegin(GL_POINTS);
        glVertex2d(x_cen+x, y_cen+y);
        glVertex2d(x_cen-x, y_cen+y);
        glVertex2d(x_cen+x, y_cen-y);
        glVertex2d(x_cen-x, y_cen-y);
        glVertex2d(x_cen+y, y_cen+x);
        glVertex2d(x_cen-y, y_cen+x);
        glVertex2d(x_cen+y, y_cen-x);
        glVertex2d(x_cen-y, y_cen-x);
    glEnd();
    glFlush();
    usleep(100000);
}

void Ark(int x, int y) {
    int p = 1 - radius;
    glClear(GL_COLOR_BUFFER_BIT);
    while(x < y) {
        x++;
        if(p < 0)
            p += 2 * x + 1;
        else {
            y--;
            p += 2 * (x-y) + 1;
        }
        draw_point_circle(x, y);
        //usleep(10000);
    }
}

void Circle() {
    Ark(0, radius);
}

void Init() {
  glClearColor(1.0,1.0,1.0,0);
  glColor3f(0.0,0.0,0.0);
  gluOrtho2D(0 , 640 , 0 , 480);
}

int main(int argc, char **argv) {
  printf("Enter radius of circle to be drawn:\n");
  printf("\n************************************\n");
  scanf("%d",&radius);

  printf("Enter midpoint of circle to be drawn:\n");
  printf("\n************************************\n");
  scanf("%d%d",&x_cen, &y_cen);
  
  glutInit(&argc,argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowPosition(0,0);
  glutInitWindowSize(640,480);
  glutCreateWindow("Circle drawing");
  Init();
  glutDisplayFunc(Circle);
  glutMainLoop();
  return 0;
}
```
## Output
radius = 100; center = (200, 200) <br>
![Practical 4 Output 1](https://imgur.com/mas4yfQ.png)
<br><br>
radius = 80; center = (150, 250) <br>
![Practical 4 Output 2](https://imgur.com/h0nGZyM.png)

## Discussion
The purpose of Bresenham’s circle algorithm is to generate the set of
points that approximate a circle on a pixel-based display. We generate the
points in the first octant of the circle and then use symmetry to generate
the other seven octants. We start at (r, 0). This pixel is chosen trivially.
Next we need to find the pixel for the y = 1 row. We must make a decision
between two candidate points.

## Finding and Learning
Implemented Bresenham Circle drawing algorithm using OpenGL in C.
