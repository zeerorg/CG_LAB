<div align="center">
<h1> Practical 5 </h1>
</div>

## Aim
To write a program which draws circle with Bresenham's circle algorithm.

## Description of aim and related theory
In mathematics, an ellipse is a curve in a plane surrounding two focal points such that the sum of the distances to the two focal points is constant for every point on the curve. As such, it is a generalization of a circle, which is a special type of an ellipse having both focal points at the same location. The shape of an ellipse (how "elongated" it is) is represented by its eccentricity, which for an ellipse can be any number from 0 (the limiting case of a circle) to arbitrarily close to but less than 1.

## Algorithm
1. Set RXSq = RX * RX
2. Set RYSq = RY * RY
3. Set X = 0 and Y = RY
4. Set PX = 0 and PY = 2 * RXSq * Y
5. Call Draw Elliplse(XC, YC, X, Y)
6. Set P = RYSq – (RXSq * RY) + (0.25 * RXSq) [Region 1]
7. Repeat While (PX < PY)
8. &emsp;&emsp;Set X = X + 1
9. &emsp;&emsp;PX = PX + 2 * RYSq
10. &emsp;&emsp;If (P < 0) Then
11. &emsp;&emsp;&emsp;&emsp;Set P = P + RYSq + PX
12. &emsp;&emsp;Else
13. &emsp;&emsp;&emsp;&emsp;Set Y = Y – 1
14. &emsp;&emsp;&emsp;&emsp;Set PY = PY – 2 * RXSq
15. &emsp;&emsp;&emsp;&emsp;Set P = P + RYSq + PX – PY
16. &emsp;&emsp;End If
16. &emsp;&emsp;Call Draw Elliplse(XC, YC, X, Y)
18. End While
17. Set P = RYSq*(X + 0.5)2+RXSq*(Y – 1)2–RXSq*RYSq [Region 2]
18. Repeat While (Y > 0)
19. &emsp;&emsp;Set Y = Y – 1
20. &emsp;&emsp;Set PY = PY – 2 * RXSq
21. &emsp;&emsp;If (P > 0) Then
22. &emsp;&emsp;&emsp;&emsp;Set P = P + RXSq – PY
23. &emsp;&emsp;Else
24. &emsp;&emsp;&emsp;&emsp;Set X = X + 1
25. &emsp;&emsp;&emsp;&emsp;Set PX + 2 * RYSq
26. &emsp;&emsp;&emsp;&emsp;Set P = P + RXSq – PY + PX
29. &emsp;&emsp;End If
27. &emsp;&emsp;Call Draw Ellipse(XC, YC, X, Y)
30. End While
28. Exit

## Code
```C
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <GL/glut.h>

#define ROUND(a) ((int) (a+0.5))

int Rx, Ry, x_cen, y_cen;

void draw_point_ellipse(int x, int y) {
    glBegin(GL_POINTS);
        glVertex2d(x_cen+x, y_cen+y);
        glVertex2d(x_cen-x, y_cen+y);
        glVertex2d(x_cen+x, y_cen-y);
        glVertex2d(x_cen-x, y_cen-y);
    glEnd();
    glFlush();
    usleep(10000);
}

void Ark() {
    int Rx2 = Rx * Rx;
    int Ry2 = Ry * Ry;
    int twoRx2 = 2 * Rx2;
    int twoRy2 = 2 * Ry2;
    int p;
    int x = 0;
    int y = Ry;
    int px = 0;
    int py = twoRx2 * y;
    glClear(GL_COLOR_BUFFER_BIT);

    draw_point_ellipse(x, y);
    p = ROUND (Ry2 - (Rx2 * Ry) + (0.25 * Rx2));
    /* Region1 */
    while(px < py) {
        x++;
        px += twoRy2;
        if(p < 0)
            p += Ry2 + px;
        else {
            y--;
            py -= twoRx2;
            p += Ry2 + px - py;
        }
        draw_point_ellipse(x, y);
        //usleep(10000);
    }

    /* Region 2 */
    p = ROUND (Ry2 * (x+0.5) * (x+0.5) + Rx2*(y-1)*(y-1) - Rx2*Ry2);
    while(y > 0) {
        y--;
        py -= twoRx2;
        if(p > 0)
            p += Rx2 - py;
        else {
            x++;
            px += twoRy2;
            p += Rx2 - py + px;
        }
        draw_point_ellipse(x, y);
    }
}

void Ellipse() {
    Ark();
}

void Init() {
  glClearColor(1.0,1.0,1.0,0);
  glColor3f(0.0,0.0,0.0);
  gluOrtho2D(0 , 640 , 0 , 480);
}

int main(int argc, char **argv) {
  printf("Enter poles of ellipse to be drawn:\n");
  printf("\n************************************\n");
  scanf("%d%d",&Rx, &Ry);

  printf("Enter midpoint of ellipse to be drawn:\n");
  printf("\n***********************************\n");
  scanf("%d%d",&x_cen, &y_cen);
  
  glutInit(&argc,argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowPosition(0,0);
  glutInitWindowSize(640,480);
  glutCreateWindow("Circle drawing");
  Init();
  glutDisplayFunc(Ellipse);
  glutMainLoop();
  return 0;
}
```
## Output
poles = (80, 50); center = (100, 100) <br>
![Practical 5 Output 1](https://imgur.com/BjMsyAW.png)
<br><br>
poles = (40, 80); center = (150, 250) <br>
![Practical 4 Output 2](https://imgur.com/8NW3cXd.png)

## Discussion
Midpoint ellipse algorithm is a method for drawing ellipses in computer graphics.
This method is modified from Bresenham’s algorithm. The advantage of this
modified method is that only addition operations are required in the program
loops. This leads to simple and fast implementation in all processors.

## Finding and Learning
Implemented MidPoint ellipse drawing algorithm using OpenGL in C.
