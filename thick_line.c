#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <GL/glut.h>
// gcc -Wall -g light.c -lGL -lGLU -lglut
float X1, Y1, X2, Y2;
int thickness;

int round_value(float v)
{
    return (int)v + 0.5;
}

void lin(void) {
    glClear(GL_COLOR_BUFFER_BIT);
    glLineWidth((int)thickness);
    glBegin(GL_LINES);
    glVertex2f(X1, Y1);
    glVertex2f(X2, Y2);
    glEnd();
    glFlush();
}
void Init()
{
    glClearColor(1.0,1.0,1.0,0);
    glColor3f(0.0,0.0,0.0);
    gluOrtho2D(0 , 640 , 0 , 480);
}
int main(int argc, char **argv)
{
    printf("Enter two end points of the line to be drawn:\n");
    printf("\n************************************");
    printf("\nEnter Point1( X1 , Y1):\n");
    scanf("%g%g",&X1,&Y1);
    printf("\n************************************");
    printf("\nEnter Point1( X2 , Y2):\n");
    scanf("%g%g",&X2,&Y2);
    printf("\n************************************");
    printf("\nEnter Thickness: ");
    scanf("%d",&thickness);

    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowPosition(0,0);
    glutInitWindowSize(640,480);
    glutCreateWindow("Thick Line");
    Init();
    glutDisplayFunc(lin);
    glutMainLoop();
    return 0;
}
