#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
// gcc -Wall -g light.c -lGL -lGLU -lglut
int ac;
char **av;
int DETECT = 5;


void disp() {
  
  glClear(GL_COLOR_BUFFER_BIT);

  glBegin(GL_LINES);
    glVertex2i(100, 100);
    glVertex2i(200, 200);
  glEnd();

  glFlush();
}
void Init() {
  glClearColor(1.0,1.0,1.0,0);
  glColor3f(0.0,0.0,0.0);
  gluOrtho2D(0 , 640 , 0 , 480);
}

void intgraph(int *gd, int *gm, char *s) {
  glutInit(&ac,av);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowPosition(0,0);
  glutInitWindowSize(640,480);
  glutCreateWindow("Line");
  Init();
}

void line(int x1, int y1, int x2, int y2) {
    glutDisplayFunc(disp);
}

int main(int argc, char **argv)
{
  ac = argc;
  av = argv;
  int gd = DETECT, gm = 0 ;
  intgraph(&gd, &gm, "C:\\TC\\BGI");
  line(100, 100, 200, 200);
  glutMainLoop();
  return 0;
}