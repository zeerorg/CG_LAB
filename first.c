#include<stdio.h>
#include<graphics.h>
#include<stdlib.h>
#include<X11/Xlib.h>
#define ROUND(a) ((int)(a+0.5))
void setpixel(int x, int y, int X, int Y) {
	//printf("%d %d\n", x, y);
	int val = 10;
	putpixel(x, y, val);
	putpixel(x+X, y+Y, val);
	putpixel(x-X, y-Y, val);
}

void thickline(int x1, int y1, int x2, int y2) {
	float xsteps, ysteps, x=x1, y=y1;
	int X = 0, Y=0;
	int dx = x2 - x1;
	int dy = y2 - y1;
	int steps, k = 1;
	if(abs(dx) >= abs(dy)) {
		steps = abs(dy);
		Y = 1;
	} else {
		steps = abs(dy);
		X = 1;
	}

	xsteps = dx / (float)steps;
	ysteps = dy/ (float)steps;

	setpixel( ROUND(x), ROUND(y), X, Y );

	while( k <= steps ) {
		x = x + xsteps;
		y = y + ysteps;
		setpixel(ROUND(x), ROUND(y), X, Y);
		k++;
	}
}

int main() {
	XInitThreads();
	int x1, x2, y1, y2;
	int g_driver = DETECT, g_mode, error_code;
	initgraph(&g_driver, &g_mode, "NULL");

	//printf(" Enter strat point: ");
	//scanf("%d %d", &x1, &y1);
	//printf("Enter second point: ");
	//scanf("%d %d", &x2, &y2);
	x1 = 10;
	y1 = 20;
	x2 = 10;
	y2 = 100;
	thickline(x1, y1, x2, y2);
	int in = 0;
	while (in == 0) {
    		in = getchar();
	}
	closegraph();
	return 0;
}
