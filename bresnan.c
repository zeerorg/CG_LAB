#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <GL/glut.h>
// gcc -Wall -g light.c -lGL -lGLU -lglut
int X1, Y1, X2, Y2;

void LineBresenham() {
    int x1 = X1, y1 = Y1, x2 = X2, y2 = Y2;
    int m = 2 * (y2 - y1);
    int slope_err = m - (x2 - x1);
    glClear(GL_COLOR_BUFFER_BIT);
    for(int x=x1, y=y1; x <= x2; x++) {
        glBegin(GL_POINTS);
            glVertex2d(x, y);
        glEnd();
        glFlush();
        usleep(10000);
        slope_err += m;
        if(slope_err >= 0) {
            y++;
            slope_err -= 2 * (x2 - x1);
        }
    }
}

void Init() {
  glClearColor(1.0,1.0,1.0,0);
  glColor3f(0.0,0.0,0.0);
  gluOrtho2D(0 , 640 , 0 , 480);
}

int main(int argc, char **argv) {
  printf("Enter two end points of the line to be drawn:\n");
  printf("\n************************************");
  printf("\nEnter Point1( X1 , Y1):\n");
  scanf("%d%d",&X1,&Y1);
  printf("\n************************************");
  printf("\nEnter Point1( X2 , Y2):\n");
  scanf("%d%d",&X2,&Y2);
  
  glutInit(&argc,argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowPosition(0,0);
  glutInitWindowSize(640,480);
  glutCreateWindow("Bresnan Line Algo");
  Init();
  glutDisplayFunc(LineBresenham);
  glutMainLoop();
  return 0;
}