#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <GL/glut.h>
// gcc -Wall -g circle.c -lGL -lGLU -lglut
int radius, x_cen, y_cen;

void draw_point_circle(int x, int y) {
    glBegin(GL_POINTS);
        glVertex2d(x_cen+x, y_cen+y);
        glVertex2d(x_cen-x, y_cen+y);
        glVertex2d(x_cen+x, y_cen-y);
        glVertex2d(x_cen-x, y_cen-y);
        glVertex2d(x_cen+y, y_cen+x);
        glVertex2d(x_cen-y, y_cen+x);
        glVertex2d(x_cen+y, y_cen-x);
        glVertex2d(x_cen-y, y_cen-x);
    glEnd();
    glFlush();
    usleep(100000);
}

void Ark(int x, int y) {
    int p = 1 - radius;
    glClear(GL_COLOR_BUFFER_BIT);
    while(x < y) {
        x++;
        if(p < 0)
            p += 2 * x + 1;
        else {
            y--;
            p += 2 * (x-y) + 1;
        }
        draw_point_circle(x, y);
        //usleep(10000);
    }
}

void Circle() {
    Ark(0, radius);
}

void Init() {
  glClearColor(1.0,1.0,1.0,0);
  glColor3f(0.0,0.0,0.0);
  gluOrtho2D(0 , 640 , 0 , 480);
}

int main(int argc, char **argv) {
  printf("Enter radius of circle to be drawn:\n");
  printf("\n************************************\n");
  scanf("%d",&radius);

  printf("Enter midpoint of circle to be drawn:\n");
  printf("\n************************************\n");
  scanf("%d%d",&x_cen, &y_cen);
  
  glutInit(&argc,argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowPosition(0,0);
  glutInitWindowSize(640,480);
  glutCreateWindow("Circle drawing");
  Init();
  glutDisplayFunc(Circle);
  glutMainLoop();
  return 0;
}